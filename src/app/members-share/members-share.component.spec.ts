import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MembersShareComponent } from './members-share.component';

describe('MembersShareComponent', () => {
  let component: MembersShareComponent;
  let fixture: ComponentFixture<MembersShareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MembersShareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MembersShareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
