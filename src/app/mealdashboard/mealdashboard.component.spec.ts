import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MealdashboardComponent } from './mealdashboard.component';

describe('MealdashboardComponent', () => {
  let component: MealdashboardComponent;
  let fixture: ComponentFixture<MealdashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MealdashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MealdashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
