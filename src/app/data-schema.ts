export class TypeClass {
  token: string;
  vURL: string;
  vType: number;
  vKeyword: string;
}

export class PullDataOnly {
  token: string;
  vURL: string;
}

export class UpdateInventory {
  token: string;
  vURL: string;
  a_linr: string;
  a_pa: number;
  a_paf: number;
  a_pn: number;
  a_pu: number;
  a_sucmi: number;
  a_totalacquired: number;
  a_totalitems: number;
  a_submittedby: string;
  a_remarks: string;
  a_status: number;
  vType: number;
  vKeyword: string;
}

export class AddRecord {
  token: string;
  vURL: string;
  n_linr: string;
  n_name: string;
  n_ui: string;
  n_type: number;
  n_keyword: string;
  i_pa: number;
  i_paf: number;
  i_pn: number;
  i_pu: number;
  i_sucmi: number;
}

export class AddPersonnel {
  token: string;
  vURL: string;
  p_fname: string; 
  p_mname: string;
  p_lname: string;
  p_nameext: string;
  p_nameprefix: string; 
  p_contactnum: string;
  p_rank: string;
  p_unit: string;
  p_badgenum: string;
  p_username: string;
  p_password: string;
  p_role: number;
}

export class AppPersonnelLogin {
  token: string;
  vURL: string;
  p_username: string;
  p_password: string;
}

export class ApproveSubmittedInventory {
  token: string;
  vURL: string;
  a_linr: string;
  i_pa: number;
  i_paf: number;
  i_pn: number;
  i_pu: number;
  i_sucmi: number;
  a_status: number;
  a_approvedby: string;
}