import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { PullDataOnly } from '../data-schema';

@Component({
  selector: 'app-generate-report',
  templateUrl: './generate-report.component.html',
  styleUrls: ['./generate-report.component.scss']
})
export class GenerateReportComponent implements OnInit {
  dateToday: number;
  vData: Object;
  recordCounter: number;
  tk: Object;
  vCategories: string[];
  pd = new PullDataOnly();
  vMonths: string[];
  myDate: string;
  overAllTotal: number;
  subTotal: number;
  totalPA: number;
  totalPN: number;
  totalPAF: number;
  totalPU: number;
  totalSUCMI: number;

  overAllPA: number;
  overAllPN: number;
  overAllPU: number;
  overAllPAF: number;
  overAllSUCMI: number;
  


  constructor(private ds: DataService) { 
    this.vCategories = [
      'Pistols',
      'Sub-Machine Guns',
      'Carbines',
      'Rifles',
      'Sniper Rifles',
      'Machine Guns',
      'Grenade Launchers',
      'Recoilless Rifles',
      'Rocket Launchers',
      'Artillery Weapons',
      'PA Peculiar Weapons',
      'PN Peculiar Weapons',
      'PAF Peculiar Weapons',
      'Shot Guns',
      'Revolvers',
      'Non-Standard Weapons'
    ];

    this.vMonths = [
      'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
    ];
  }

  ngOnInit() {
    // var mdate = new Date();
    // this.dateToday = Date.now();
    // this.overAllPA=this.overAllPAF=this.overAllPN=this.overAllPU=this.overAllSUCMI=this.overAllTotal=0;
    // this.myDate = this.vMonths[mdate.getMonth()] + ' ' + mdate.getDate().toString() + ", " + mdate.getFullYear().toString();
    // this.tk = JSON.parse(localStorage.getItem('tmcore'));
    // this.pd.token = this.tk[0].token;
    // this.pd.vURL = this.tk[0].vURL;
    // this.ds.pullData(this.pd, "allrecords").subscribe((res)=>{
    //   this.vData = res[0].result;
    //   this.recordCounter = res[0].result.length;
    //   console.log(this.vData);
    // });
  }

  rowTotal(a, b, c, d, e) {
    if(a=="NaN" || a=="")
      a = 0;
    if(b=="NaN" || b=="")
      b = 0;
    if(c=="NaN" || c=="")
      c = 0;
    if(d=="NaN" || d=="")
      d = 0;
    if(e=="NaN" || e=="")
      e = 0;
    return parseInt(a) + parseInt(b) + parseInt(c) + parseInt(d) + parseInt(e);
  }

  print() : void {

    let vHead = `
      <head>
        <title>Armed Forces of the Philippines - Munitions Control Center</title>
        <style>
          body {
            margin: 10mm 10mm 10mm 10mm;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 10px
          }

          .print-headers {
            text-align: center; font-weight: bold; 
          }

          .column-headers {
            text-align: center; font-size: 10px;
          }

          .table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 1rem;
            background-color: transparent;
            vertical-align: middle;
          }

          .table-bordered th, .table-bordered td {
            border: 1px solid #dee2e6;
          }

          .columnCenter {
            text-align: center;
          }
          
          .recordContent {
            font-size: 10px;
          }

          td {
            padding: 7px 3px;
            vertical-align: middle;
          }
        </style>
      </head>`;
    
    let tblString = ``;
    for(var cat=0; cat<this.vCategories.length; cat++){
      this.totalPA = this.totalPAF = this.totalPN = this.totalPU = this.totalSUCMI = this.subTotal = 0;
      tblString+=`<p class="classSubHead">Classification: <strong>`+this.vCategories[cat]+`</strong></p>`
      tblString+=`<table class="table table-bordered">`;
      tblString+=`<thead>
          <th  class="column-headers" style="width: 5%">Li/Nr</th>
          <th  class="column-headers" style="width: 30%">Nomenclature</th>
          <th  class="column-headers" style="width: 5%">UI</th>
          <th  class="column-headers" style="width: 10%">PA</th>
          <th  class="column-headers" style="width: 10%">PAF</th>
          <th  class="column-headers" style="width: 10%">PN</th>
          <th  class="column-headers" style="width: 10%">Program 4 Units</th>
          <th  class="column-headers" style="width: 10%">SU-CMI</th>
          <th  class="column-headers" style="width: 10%">Total On-hand</th>
        </thead>`;
      
      tblString+=`<tbody>`;
      for(var i=0; i<this.recordCounter; i++){
        if(this.vData[i].n_type==cat+1){
          this.totalPA+=parseInt(this.vData[i].i_pa);
          this.totalPAF+=parseInt(this.vData[i].i_paf);
          this.totalPN+=parseInt(this.vData[i].i_pn);
          this.totalPU+=parseInt(this.vData[i].i_pu);
          this.totalSUCMI+=parseInt(this.vData[i].i_sucmi);
          this.subTotal+=+this.rowTotal(this.vData[i].i_pa,this.vData[i].i_paf,this.vData[i].i_pn,this.vData[i].i_pu,this.vData[i].i_sucmi);
          tblString+=`<tr>`;
          tblString+=`<td class="recordContent columnCenter">`+this.vData[i].n_linr+`</td>`;
          tblString+=`<td class="recordContent">`+this.vData[i].n_name+`</td>`;
          tblString+=`<td class="recordContent columnCenter">`+this.vData[i].n_ui+`</td>`;
          tblString+=`<td class="recordContent columnCenter">`+this.vData[i].i_pa+`</td>`;
          tblString+=`<td class="recordContent columnCenter">`+this.vData[i].i_paf+`</td>`;
          tblString+=`<td class="recordContent columnCenter">`+this.vData[i].i_pn+`</td>`;
          tblString+=`<td class="recordContent columnCenter">`+this.vData[i].i_pu+`</td>`;
          tblString+=`<td class="recordContent columnCenter">`+this.vData[i].i_sucmi+`</td>`;
          tblString+=`<td class="recordContent columnCenter">`+this.rowTotal(this.vData[i].i_pa,this.vData[i].i_paf,this.vData[i].i_pn,this.vData[i].i_pu,this.vData[i].i_sucmi)+`</td>`;
          tblString+=`</tr>`;
        }
      }
      tblString+=`<tr><td class="recordContent" colspan="3" style="text-align: right"><strong>Total</strong></td>`;
      tblString+=`<td class="recordContent columnCenter">`+this.totalPA+`</td>`;
      tblString+=`<td class="recordContent columnCenter">`+this.totalPAF+`</td>`;
      tblString+=`<td class="recordContent columnCenter">`+this.totalPN+`</td>`;
      tblString+=`<td class="recordContent columnCenter">`+this.totalPU+`</td>`;
      tblString+=`<td class="recordContent columnCenter">`+this.totalSUCMI+`</td>`;
      tblString+=`<td class="recordContent columnCenter">`+this.subTotal+`</td></tr>`;
      tblString+=`</tbody>`;
      tblString+=`</table><br><br>`;
      this.overAllPA+=this.totalPA;
      this.overAllPAF+=this.totalPAF;
      this.overAllPN+=this.totalPN;
      this.overAllPU+=this.totalPU;
      this.overAllSUCMI+=this.totalSUCMI;
    }
    this.overAllTotal=this.overAllPA+this.overAllPAF+this.overAllPN+this.overAllPU+this.overAllSUCMI;
    let tblShowTotal=``;
    tblShowTotal+=`<table class="table table-bordered" style="width:60% !important">`;
    tblShowTotal+=`<thead><th class="column-headers">Unit</th><th class="column-headers">Total Weapons</th></thead>`;
    tblShowTotal+=`<tbody>`;
    tblShowTotal+=`<tr><td class="recordContent">Philippine Army</td><td class="recordContent" style="text-align: right">`+this.overAllPA+`</td></tr>`;
    tblShowTotal+=`<tr><td class="recordContent">Philippine Air Force</td><td class="recordContent" style="text-align: right">`+this.overAllPAF+`</td></tr>`;
    tblShowTotal+=`<tr><td class="recordContent">Philippine Navy</td><td class="recordContent" style="text-align: right">`+this.overAllPN+`</td></tr>`;
    tblShowTotal+=`<tr><td class="recordContent">Program 4 Units</td><td class="recordContent" style="text-align: right">`+this.overAllPU+`</td></tr>`;
    tblShowTotal+=`<tr><td class="recordContent">SU-CMI</td><td class="recordContent" style="text-align: right">`+this.overAllSUCMI+`</td></tr>`;
    tblShowTotal+=`<td style="text-align: center"><strong>Overall Total</strong></td><td style="text-align: right">`+this.overAllTotal+`</td>`;
    tblShowTotal+=`</tbody>`;
    tblShowTotal+=`</table>`;

    let signatories = ``;
    signatories+=`
      <br><br><br><br>
      <div>
        Prepared by:<br><br><br><br>________________________________ (Rank and Name)
        <br><br>________________________________ (Unit)
        <br><br>________________________________ (Date)
      </div>
      <br><br><br><br>
      <div>
        Submitted to:<br><br><br><br>________________________________ (Rank and Name)
        <br><br>________________________________ (Unit)
        <br><br>________________________________ (Date)
      </div>
      <br><br><br><br>
      <div>
        Approved by:<br><br><br><br>________________________________ (Rank and Name)
        <br><br>________________________________ (Unit)
        <br><br>________________________________ (Date)
      </div>
    `;

    let vBody = `
    <body onload="window.print();window.close()">
      <h3 class="print-headers">
        Consolidated AFP-Wide Weapons Inventory Report<br> as of `+this.myDate+`
      </h3><br>`+ tblString+tblShowTotal+signatories+`</body>`;

    
    
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`<html>`+vHead+vBody+`</html>`);
    popupWin.document.close();
  }

}
