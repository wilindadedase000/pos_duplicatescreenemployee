import { Routes } from '@angular/router';


import { MealreportComponent } from '../../mealreport/mealreport.component';
import { MealdashboardComponent } from '../../mealdashboard/mealdashboard.component';

export const MealLayoutRoutes: Routes = [

    { path: 'mealreport',component: MealreportComponent },
    { path: 'mealdashboard',component: MealdashboardComponent }
    

];

