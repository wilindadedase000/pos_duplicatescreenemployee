import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MealLayoutRoutes } from './meal-layout.routing';
import { ChartsModule } from 'ng2-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';

//components

import { MealreportComponent } from '../../mealreport/mealreport.component';
import { MealdashboardComponent } from '../../mealdashboard/mealdashboard.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(MealLayoutRoutes),
    FormsModule,
    ChartsModule,
    NgbModule,
    ToastrModule.forRoot()
  ],
  declarations: [
    MealreportComponent, 
    MealdashboardComponent

  ]
})

export class MealLayoutModule {}