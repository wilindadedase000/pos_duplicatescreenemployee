import { Routes } from '@angular/router';

// import { DashboardComponent } from '../../dashboard/dashboard.component';
import { MealreportComponent } from '../../mealreport/mealreport.component';
import { MealdashboardComponent } from '../../mealdashboard/mealdashboard.component';



export const AdminLayoutRoutes: Routes = [
    // { path: 'dashboard',component: DashboardComponent },
    { path: 'mealreport',component: MealreportComponent },
    { path: 'mealdashboard',component: MealdashboardComponent }
];
