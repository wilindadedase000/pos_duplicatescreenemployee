import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { ChartsModule } from 'ng2-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';

//components
// import { DashboardComponent } from '../../dashboard/dashboard.component';
import { MealreportComponent } from '../../mealreport/mealreport.component';
import { MealdashboardComponent } from '../../mealdashboard/mealdashboard.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ChartsModule,
    NgbModule,
    ToastrModule.forRoot()
  ],
  declarations: [
    // DashboardComponent, 
    MealreportComponent, 
    MealdashboardComponent
  ]
})

export class AdminLayoutModule {}
