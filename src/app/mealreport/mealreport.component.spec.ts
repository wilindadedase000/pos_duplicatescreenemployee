import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MealreportComponent } from './mealreport.component';

describe('MealreportComponent', () => {
  let component: MealreportComponent;
  let fixture: ComponentFixture<MealreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MealreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MealreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
