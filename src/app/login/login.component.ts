import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  // @ViewChild('username') username;
  // @ViewChild('password') password;
  username:string;
  password:string;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  signIn(e){
    // console.log(this.username.value, this.password.value);
    e.preventDefault();
    this.username = e.target[0].value;
    this.password = e.target[1].value;
    // console.log(this.username, this.password);
    if(this.username == 'meal'){
     this.router.navigate(['mealdashboard']);
    }
   
    else{
      alert('Unauthorized account');
    }
    
  }

}
