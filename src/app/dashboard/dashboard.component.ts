import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { PullDataOnly,TypeClass } from '../data-schema';

import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  closeResult: string;
  record:number;


  constructor(private ds: DataService, private modalService: NgbModal) {  }

  ngOnInit(){ }

  processData() {
   
  }
  
  open(content){
    this.modalService.open(content, {'size': 'lg', 'centered': true }).result.then((result)=>{
      this.closeResult = 'Closed with: ${result}';
    }, (reason) =>{
      this.closeResult = 'Dismissed ${this.getDismissReason(reason)}';
    });
  }
  
  private getDismissReason(reason: any) : string {
    if(reason === ModalDismissReasons.ESC) {
      return 'by Pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by Clicking on a Backdrop';
    } else {
      return 'with: ${reasonm}';
    }
  }
}
